#include<stdio.h>
#include<stdlib.h>
#include"moduly.h"

void addClient(struct Client **head)
{
    printf("addClient()\n\n");
    int numOfClientsToAdd = 0;
    printf("Input number of clients You wish to add:\n");
    scanf("%d", &numOfClientsToAdd);
    char surname[25];
    int i= 0;
    for (i = 0; i<numOfClientsToAdd; i++)
    {
        printf("Input name of new client: \n");
        scanf("%s", &surname);
        push_back(head, surname);
    }
}

void deleteClient(struct Client **head)
{
    printf("deleteClient()\n\n");
    char surname[25];
    printf("Input name of client to delete: \n");
    scanf("%s", &surname);
    pop_by_surname(head, surname);
}

int main ()
{
    struct Client **head;
    head = (struct Client*)malloc(sizeof(struct Client));
    head = NULL;
    int i = 0;
    for (i = 0; i<200; i++)
    {
        printf("Choose Action:\n");
        printf("Add clients:\t\t - 1\n");
        printf("Delete client:\t\t - 2\n");
        printf("Show number of clients:\t - 3\n");
        printf("Show client surnames:\t - 4\n");
        printf("Exit\t - any remaining\n");

        int choice = 0;
        scanf("%d",&choice);
        if (choice == 1)
        {
            addClient(&head);
        }
        else if (choice == 2)
        {
            deleteClient(&head);
        }
        else if (choice == 3)
        {
            printf("%d", list_size(head));
        }
        else if (choice == 4)
        {
            show_list(head);
        }
        else
        {
            exit(0);
        }
    }
    return 0;
}
